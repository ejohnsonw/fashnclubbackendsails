/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     AssetQueryDeep.js
Description:  Sails 0.11.x does not populate records more than 1 level deep. This service navigates the hierarchy and goes down one more level
Project:      Fashn
Template: /sails-backend-0.11.x/services/sails.populateDeep.vm
 */

var AssetPopulateDeepService = {
    populate: function(id) {
        async.auto({
                // First get the master  
                asset: function(cb) {
                    Asset.findOne({
                        where: {
                            id: id
                        }
                    }).populate("image").populate("composition").exec(cb);
                }


                //Relations for image
                ,
                imageCollectionsForimage: ['asset',
                    function(cb, results) {
                        ImageCollection.find({
                            id: _.pluck(results.asset.image, 'imageCollection')
                        }).exec(cb);
                    }
                ],
                imageDeep: ['imageCollectionsForimage',
                    function(cb, results) {

                        var imageCollections = _.indexBy(results.imageCollectionsForimage, 'id');

                        var asset = results.asset.toObject();
                        asset.imageMapped = _.map(asset.image, function(image) {
                            image.imageCollection = imageCollections[image.imageCollection];
                            return image;
                        });

                        return cb(null, asset);
                    }
                ]


                //Relations for composition
                ,
                usersForcomposition: ['asset',
                    function(cb, results) {
                        User.find({
                            id: _.pluck(results.asset.composition, 'user')
                        }).exec(cb);
                    }
                ],
                compositionDeep: ['usersForcomposition',
                    function(cb, results) {

                        var users = _.indexBy(results.usersForcomposition, 'id');

                        var asset = results.asset.toObject();
                        asset.compositionMapped = _.map(asset.composition, function(composition) {
                            composition.user = users[composition.user];
                            return composition;
                        });

                        return cb(null, asset);
                    }
                ]
            },
            function finish(err, results) {
                if (err) {
                    return res.serverError(err);
                }
                console.log(results);
                res.json(results);
            }

        );
    }
};
module.exports = AssetPopulateDeepService;


/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 22.9 minutes to type the 2290+ characters in this file.
 */