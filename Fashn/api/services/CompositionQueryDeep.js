/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     CompositionQueryDeep.js
Description:  Sails 0.11.x does not populate records more than 1 level deep. This service navigates the hierarchy and goes down one more level
Project:      Fashn
Template: /sails-backend-0.11.x/services/sails.populateDeep.vm
 */

var CompositionPopulateDeepService = {
    populate: function(id) {
        async.auto({
                // First get the master  
                composition: function(cb) {
                    Composition.findOne({
                        where: {
                            id: id
                        }
                    }).populate("assets").populate("user").exec(cb);
                }


                //Relations for assets
                ,
                imagesForassets: ['composition',
                    function(cb, results) {
                        Image.find({
                            id: _.pluck(results.composition.assets, 'image')
                        }).exec(cb);
                    }
                ],
                assetsDeep: ['imagesForassets',
                    function(cb, results) {

                        var images = _.indexBy(results.imagesForassets, 'id');

                        var composition = results.composition.toObject();
                        composition.assetsMapped = _.map(composition.assets, function(asset) {
                            asset.image = images[asset.image];
                            return asset;
                        });

                        return cb(null, composition);
                    }
                ]


                //Relations for user
                ,
                imageCollectionssForuser: ['composition',
                    function(cb, results) {
                        ImageCollection.find({
                            id: _.pluck(results.composition.user, 'imageCollections')
                        }).exec(cb);
                    }
                ],
                userDeep: ['imageCollectionssForuser',
                    function(cb, results) {

                        var imageCollectionss = _.indexBy(results.imageCollectionssForuser, 'id');

                        var composition = results.composition.toObject();
                        composition.userMapped = _.map(composition.user, function(user) {
                            user.imageCollections = imageCollectionss[user.imageCollections];
                            return user;
                        });

                        return cb(null, composition);
                    }
                ]
            },
            function finish(err, results) {
                if (err) {
                    return res.serverError(err);
                }
                console.log(results);
                res.json(results);
            }

        );
    }
};
module.exports = CompositionPopulateDeepService;


/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 23.56 minutes to type the 2356+ characters in this file.
 */