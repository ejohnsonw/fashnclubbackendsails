/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     ImageCollectionQueryDeep.js
Description:  Sails 0.11.x does not populate records more than 1 level deep. This service navigates the hierarchy and goes down one more level
Project:      Fashn
Template: /sails-backend-0.11.x/services/sails.populateDeep.vm
 */

var ImageCollectionPopulateDeepService = {
    populate: function(id) {
        async.auto({
                // First get the master  
                imageCollection: function(cb) {
                    ImageCollection.findOne({
                        where: {
                            id: id
                        }
                    }).populate("user").populate("images").exec(cb);
                }


                //Relations for user
                ,
                compositionssForuser: ['imageCollection',
                    function(cb, results) {
                        Composition.find({
                            id: _.pluck(results.imageCollection.user, 'compositions')
                        }).exec(cb);
                    }
                ],
                userDeep: ['compositionssForuser',
                    function(cb, results) {

                        var compositionss = _.indexBy(results.compositionssForuser, 'id');

                        var imageCollection = results.imageCollection.toObject();
                        imageCollection.userMapped = _.map(imageCollection.user, function(user) {
                            user.compositions = compositionss[user.compositions];
                            return user;
                        });

                        return cb(null, imageCollection);
                    }
                ]


                //Relations for images
                ,
                imagesDeep: [,
                    function(cb, results) {


                        var imageCollection = results.imageCollection.toObject();
                        imageCollection.imagesMapped = _.map(imageCollection.images, function(image) {
                            return image;
                        });

                        return cb(null, imageCollection);
                    }
                ]
            },
            function finish(err, results) {
                if (err) {
                    return res.serverError(err);
                }
                console.log(results);
                res.json(results);
            }

        );
    }
};
module.exports = ImageCollectionPopulateDeepService;


/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 19.46 minutes to type the 1946+ characters in this file.
 */