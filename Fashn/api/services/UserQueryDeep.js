/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     UserQueryDeep.js
Description:  Sails 0.11.x does not populate records more than 1 level deep. This service navigates the hierarchy and goes down one more level
Project:      Fashn
Template: /sails-backend-0.11.x/services/sails.populateDeep.vm
 */

var UserPopulateDeepService = {
    populate: function(id) {
        async.auto({
                // First get the master  
                user: function(cb) {
                    User.findOne({
                        where: {
                            id: id
                        }
                    }).populate("imageCollections").populate("compositions").exec(cb);
                }


                //Relations for imageCollections
                ,
                imagessForimageCollections: ['user',
                    function(cb, results) {
                        Image.find({
                            id: _.pluck(results.user.imageCollections, 'images')
                        }).exec(cb);
                    }
                ],
                imageCollectionsDeep: ['imagessForimageCollections',
                    function(cb, results) {

                        var imagess = _.indexBy(results.imagessForimageCollections, 'id');

                        var user = results.user.toObject();
                        user.imageCollectionsMapped = _.map(user.imageCollections, function(imageCollection) {
                            imageCollection.images = imagess[imageCollection.images];
                            return imageCollection;
                        });

                        return cb(null, user);
                    }
                ]


                //Relations for compositions
                ,
                assetssForcompositions: ['user',
                    function(cb, results) {
                        Asset.find({
                            id: _.pluck(results.user.compositions, 'assets')
                        }).exec(cb);
                    }
                ],
                compositionsDeep: ['assetssForcompositions',
                    function(cb, results) {

                        var assetss = _.indexBy(results.assetssForcompositions, 'id');

                        var user = results.user.toObject();
                        user.compositionsMapped = _.map(user.compositions, function(composition) {
                            composition.assets = assetss[composition.assets];
                            return composition;
                        });

                        return cb(null, user);
                    }
                ]
            },
            function finish(err, results) {
                if (err) {
                    return res.serverError(err);
                }
                console.log(results);
                res.json(results);
            }

        );
    }
};
module.exports = UserPopulateDeepService;


/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 23.78 minutes to type the 2378+ characters in this file.
 */